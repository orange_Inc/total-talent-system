$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("GetValuesAPI.feature");
formatter.feature({
  "line": 2,
  "name": "Verification of Get method in TTS Deloitte API",
  "description": "",
  "id": "verification-of-get-method-in-tts-deloitte-api",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@API"
    }
  ]
});
formatter.scenarioOutline({
  "line": 5,
  "name": "Verify the Get Method of all the System Values",
  "description": "",
  "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 4,
      "name": "@GET"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Check the get method for the System value \"\u003cname\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Should reply the following status Code \u003cvalue\u003e for this method \"\u003cname\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "line": 9,
  "name": "",
  "description": "",
  "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;",
  "rows": [
    {
      "cells": [
        "name",
        "value"
      ],
      "line": 10,
      "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;1"
    },
    {
      "cells": [
        "GetSystemName",
        "200"
      ],
      "line": 11,
      "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;2"
    },
    {
      "cells": [
        "GetCountryList",
        "200"
      ],
      "line": 12,
      "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;3"
    },
    {
      "cells": [
        "GetTimeZoneList",
        "200"
      ],
      "line": 13,
      "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;4"
    },
    {
      "cells": [
        "GetCurrencyList",
        "200"
      ],
      "line": 14,
      "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;5"
    },
    {
      "cells": [
        "GetLanguageList",
        "200"
      ],
      "line": 15,
      "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;6"
    },
    {
      "cells": [
        "GetClientJobStatusList",
        "200"
      ],
      "line": 16,
      "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;7"
    },
    {
      "cells": [
        "GetJobTypeList",
        "200"
      ],
      "line": 17,
      "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;8"
    },
    {
      "cells": [
        "GetCertificationList",
        "200"
      ],
      "line": 18,
      "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;9"
    },
    {
      "cells": [
        "GetServiceSkillList",
        "200"
      ],
      "line": 19,
      "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;10"
    },
    {
      "cells": [
        "GetManagementLevel",
        "200"
      ],
      "line": 20,
      "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;11"
    },
    {
      "cells": [
        "GetIndustry",
        "200"
      ],
      "line": 21,
      "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;12"
    },
    {
      "cells": [
        "GetSubIndustry",
        "200"
      ],
      "line": 22,
      "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;13"
    },
    {
      "cells": [
        "GetJobCategories",
        "200"
      ],
      "line": 23,
      "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;14"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 11,
  "name": "Verify the Get Method of all the System Values",
  "description": "",
  "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@API"
    },
    {
      "line": 4,
      "name": "@GET"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Check the get method for the System value \"GetSystemName\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Should reply the following status Code 200 for this method \"GetSystemName\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "GetSystemName",
      "offset": 43
    }
  ],
  "location": "GetSystemValues.check_the_get_method_for_the_System_value(String)"
});
formatter.result({
  "duration": 3836264755,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 39
    },
    {
      "val": "GetSystemName",
      "offset": 60
    }
  ],
  "location": "GetSystemValues.should_reply_the_following_status_Code_for_this_method(int,String)"
});
formatter.result({
  "duration": 2026132773,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Verify the Get Method of all the System Values",
  "description": "",
  "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@API"
    },
    {
      "line": 4,
      "name": "@GET"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Check the get method for the System value \"GetCountryList\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Should reply the following status Code 200 for this method \"GetCountryList\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "GetCountryList",
      "offset": 43
    }
  ],
  "location": "GetSystemValues.check_the_get_method_for_the_System_value(String)"
});
formatter.result({
  "duration": 2008489968,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 39
    },
    {
      "val": "GetCountryList",
      "offset": 60
    }
  ],
  "location": "GetSystemValues.should_reply_the_following_status_Code_for_this_method(int,String)"
});
formatter.result({
  "duration": 2009392452,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "Verify the Get Method of all the System Values",
  "description": "",
  "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;4",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@API"
    },
    {
      "line": 4,
      "name": "@GET"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Check the get method for the System value \"GetTimeZoneList\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Should reply the following status Code 200 for this method \"GetTimeZoneList\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "GetTimeZoneList",
      "offset": 43
    }
  ],
  "location": "GetSystemValues.check_the_get_method_for_the_System_value(String)"
});
formatter.result({
  "duration": 2214832407,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 39
    },
    {
      "val": "GetTimeZoneList",
      "offset": 60
    }
  ],
  "location": "GetSystemValues.should_reply_the_following_status_Code_for_this_method(int,String)"
});
formatter.result({
  "duration": 1975695064,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "Verify the Get Method of all the System Values",
  "description": "",
  "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;5",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@API"
    },
    {
      "line": 4,
      "name": "@GET"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Check the get method for the System value \"GetCurrencyList\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Should reply the following status Code 200 for this method \"GetCurrencyList\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "GetCurrencyList",
      "offset": 43
    }
  ],
  "location": "GetSystemValues.check_the_get_method_for_the_System_value(String)"
});
formatter.result({
  "duration": 1972938461,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 39
    },
    {
      "val": "GetCurrencyList",
      "offset": 60
    }
  ],
  "location": "GetSystemValues.should_reply_the_following_status_Code_for_this_method(int,String)"
});
formatter.result({
  "duration": 2077181125,
  "status": "passed"
});
formatter.scenario({
  "line": 15,
  "name": "Verify the Get Method of all the System Values",
  "description": "",
  "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;6",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@API"
    },
    {
      "line": 4,
      "name": "@GET"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Check the get method for the System value \"GetLanguageList\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Should reply the following status Code 200 for this method \"GetLanguageList\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "GetLanguageList",
      "offset": 43
    }
  ],
  "location": "GetSystemValues.check_the_get_method_for_the_System_value(String)"
});
formatter.result({
  "duration": 2011089216,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 39
    },
    {
      "val": "GetLanguageList",
      "offset": 60
    }
  ],
  "location": "GetSystemValues.should_reply_the_following_status_Code_for_this_method(int,String)"
});
formatter.result({
  "duration": 1938932167,
  "status": "passed"
});
formatter.scenario({
  "line": 16,
  "name": "Verify the Get Method of all the System Values",
  "description": "",
  "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;7",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@API"
    },
    {
      "line": 4,
      "name": "@GET"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Check the get method for the System value \"GetClientJobStatusList\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Should reply the following status Code 200 for this method \"GetClientJobStatusList\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "GetClientJobStatusList",
      "offset": 43
    }
  ],
  "location": "GetSystemValues.check_the_get_method_for_the_System_value(String)"
});
formatter.result({
  "duration": 1977465557,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 39
    },
    {
      "val": "GetClientJobStatusList",
      "offset": 60
    }
  ],
  "location": "GetSystemValues.should_reply_the_following_status_Code_for_this_method(int,String)"
});
formatter.result({
  "duration": 2003656014,
  "status": "passed"
});
formatter.scenario({
  "line": 17,
  "name": "Verify the Get Method of all the System Values",
  "description": "",
  "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;8",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@API"
    },
    {
      "line": 4,
      "name": "@GET"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Check the get method for the System value \"GetJobTypeList\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Should reply the following status Code 200 for this method \"GetJobTypeList\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "GetJobTypeList",
      "offset": 43
    }
  ],
  "location": "GetSystemValues.check_the_get_method_for_the_System_value(String)"
});
formatter.result({
  "duration": 1988039361,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 39
    },
    {
      "val": "GetJobTypeList",
      "offset": 60
    }
  ],
  "location": "GetSystemValues.should_reply_the_following_status_Code_for_this_method(int,String)"
});
formatter.result({
  "duration": 2009514649,
  "status": "passed"
});
formatter.scenario({
  "line": 18,
  "name": "Verify the Get Method of all the System Values",
  "description": "",
  "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;9",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@API"
    },
    {
      "line": 4,
      "name": "@GET"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Check the get method for the System value \"GetCertificationList\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Should reply the following status Code 200 for this method \"GetCertificationList\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "GetCertificationList",
      "offset": 43
    }
  ],
  "location": "GetSystemValues.check_the_get_method_for_the_System_value(String)"
});
formatter.result({
  "duration": 2245301470,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 39
    },
    {
      "val": "GetCertificationList",
      "offset": 60
    }
  ],
  "location": "GetSystemValues.should_reply_the_following_status_Code_for_this_method(int,String)"
});
formatter.result({
  "duration": 2088989870,
  "status": "passed"
});
formatter.scenario({
  "line": 19,
  "name": "Verify the Get Method of all the System Values",
  "description": "",
  "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;10",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@API"
    },
    {
      "line": 4,
      "name": "@GET"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Check the get method for the System value \"GetServiceSkillList\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Should reply the following status Code 200 for this method \"GetServiceSkillList\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "GetServiceSkillList",
      "offset": 43
    }
  ],
  "location": "GetSystemValues.check_the_get_method_for_the_System_value(String)"
});
formatter.result({
  "duration": 2603007209,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 39
    },
    {
      "val": "GetServiceSkillList",
      "offset": 60
    }
  ],
  "location": "GetSystemValues.should_reply_the_following_status_Code_for_this_method(int,String)"
});
formatter.result({
  "duration": 1995809117,
  "status": "passed"
});
formatter.scenario({
  "line": 20,
  "name": "Verify the Get Method of all the System Values",
  "description": "",
  "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;11",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@API"
    },
    {
      "line": 4,
      "name": "@GET"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Check the get method for the System value \"GetManagementLevel\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Should reply the following status Code 200 for this method \"GetManagementLevel\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "GetManagementLevel",
      "offset": 43
    }
  ],
  "location": "GetSystemValues.check_the_get_method_for_the_System_value(String)"
});
formatter.result({
  "duration": 1961847879,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 39
    },
    {
      "val": "GetManagementLevel",
      "offset": 60
    }
  ],
  "location": "GetSystemValues.should_reply_the_following_status_Code_for_this_method(int,String)"
});
formatter.result({
  "duration": 1987638978,
  "status": "passed"
});
formatter.scenario({
  "line": 21,
  "name": "Verify the Get Method of all the System Values",
  "description": "",
  "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;12",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@API"
    },
    {
      "line": 4,
      "name": "@GET"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Check the get method for the System value \"GetIndustry\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Should reply the following status Code 200 for this method \"GetIndustry\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "GetIndustry",
      "offset": 43
    }
  ],
  "location": "GetSystemValues.check_the_get_method_for_the_System_value(String)"
});
formatter.result({
  "duration": 2072734583,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 39
    },
    {
      "val": "GetIndustry",
      "offset": 60
    }
  ],
  "location": "GetSystemValues.should_reply_the_following_status_Code_for_this_method(int,String)"
});
formatter.result({
  "duration": 2028268150,
  "status": "passed"
});
formatter.scenario({
  "line": 22,
  "name": "Verify the Get Method of all the System Values",
  "description": "",
  "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;13",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@API"
    },
    {
      "line": 4,
      "name": "@GET"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Check the get method for the System value \"GetSubIndustry\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Should reply the following status Code 200 for this method \"GetSubIndustry\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "GetSubIndustry",
      "offset": 43
    }
  ],
  "location": "GetSystemValues.check_the_get_method_for_the_System_value(String)"
});
formatter.result({
  "duration": 2343505275,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 39
    },
    {
      "val": "GetSubIndustry",
      "offset": 60
    }
  ],
  "location": "GetSystemValues.should_reply_the_following_status_Code_for_this_method(int,String)"
});
formatter.result({
  "duration": 1967904145,
  "status": "passed"
});
formatter.scenario({
  "line": 23,
  "name": "Verify the Get Method of all the System Values",
  "description": "",
  "id": "verification-of-get-method-in-tts-deloitte-api;verify-the-get-method-of-all-the-system-values;;14",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@API"
    },
    {
      "line": 4,
      "name": "@GET"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Check the get method for the System value \"GetJobCategories\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Should reply the following status Code 200 for this method \"GetJobCategories\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "GetJobCategories",
      "offset": 43
    }
  ],
  "location": "GetSystemValues.check_the_get_method_for_the_System_value(String)"
});
formatter.result({
  "duration": 1981139662,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 39
    },
    {
      "val": "GetJobCategories",
      "offset": 60
    }
  ],
  "location": "GetSystemValues.should_reply_the_following_status_Code_for_this_method(int,String)"
});
formatter.result({
  "duration": 2018933725,
  "status": "passed"
});
});