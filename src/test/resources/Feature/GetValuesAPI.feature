@API
Feature: Verification of Get method in TTS Deloitte API

  @GET
  Scenario Outline: Verify the Get Method of all the System Values
    Given Check the get method for the System value "<name>"
    Then Should reply the following status Code <value> for this method "<name>"

    Examples: 
      | name                   | value |
      | GetSystemName          |   200 |
      | GetCountryList         |   200 |
      | GetTimeZoneList        |   200 |
      | GetCurrencyList        |   200 |
      | GetLanguageList        |   200 |
      | GetClientJobStatusList |   200 |
      | GetJobTypeList         |   200 |
      | GetCertificationList   |   200 |
      | GetServiceSkillList    |   200 |
      | GetManagementLevel     |   200 |
      | GetIndustry            |   200 |
      | GetSubIndustry         |   200 |
      | GetJobCategories       |   200 |