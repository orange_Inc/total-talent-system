package com.api.get;

import org.junit.Test;

import junit.framework.Assert;

@SuppressWarnings("deprecation")
public class Get {

	@Test
	public void GetSystemName() {
		Assert.assertEquals(200, GetBase.Getvalues("GetSystemName"));
	}

	@Test
	public void GetCountryList() {
		Assert.assertEquals(200, GetBase.Getvalues("GetCountryList"));
	}

	@Test
	public void GetTimeZoneList() {
		Assert.assertEquals(200, GetBase.Getvalues("GetTimeZoneList"));
	}

	@Test
	public void GetCurrencyList() {
		Assert.assertEquals(200, GetBase.Getvalues("GetCurrencyList"));
	}

	@Test
	public void GetLanguageList() {
		Assert.assertEquals(200, GetBase.Getvalues("GetLanguageList"));
	}

	@Test
	public void GetClientJobStatusList() {
		Assert.assertEquals(200, GetBase.Getvalues("GetClientJobStatusList"));
	}

	@Test
	public void GetJobTypeList() {
		Assert.assertEquals(200, GetBase.Getvalues("GetJobTypeList"));
	}

	@Test
	public void GetCertificationList() {
		Assert.assertEquals(200, GetBase.Getvalues("GetCertificationList"));
	}

	@Test
	public void GetServiceSkillList() {
		Assert.assertEquals(200, GetBase.Getvalues("GetServiceSkillList"));
	}

	@Test
	public void GetManagementLevel() {
		Assert.assertEquals(200, GetBase.Getvalues("GetManagementLevel"));
	}

	@Test
	public void GetIndustry() {
		Assert.assertEquals(200, GetBase.Getvalues("GetIndustry"));
	}

	@Test
	public void GetSubIndustry() {
		Assert.assertEquals(200, GetBase.Getvalues("GetSubIndustry"));
	}

	@Test
	public void GetJobCategories() {
		Assert.assertEquals(200, GetBase.Getvalues("GetJobCategories"));
	}

}