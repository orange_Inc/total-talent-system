package com.api.get;

import com.api.utils.Constants;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GetBase {
	
	public static int Getvalues(String arg1){
		RestAssured.baseURI = Constants.Url;
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.request(Method.GET, "DropdownList/"+arg1+"?ServiceKey="+Constants.Key);
		return response.statusCode();
	}
	

	public static String GetResposeBody(String arg1){
		RestAssured.baseURI = Constants.Url;
		RequestSpecification httpRequest = RestAssured.given();
		Response response = httpRequest.request(Method.GET, "DropdownList/"+arg1+"?ServiceKey="+Constants.Key);
		return response.getBody().asString();
	}
}
