package com.api.delete;

import com.api.utils.Constants;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class DeleteTemplate {

	public static int delete(String arg1) {
	RestAssured.baseURI = Constants.Url;
	RequestSpecification httpRequest = RestAssured.given();
	Response response = httpRequest.request(Method.GET, "DropdownList/"+arg1+"?ServiceKey="+Constants.Key);
	String responseBody = response.getBody().asString();
	System.out.println("Response Body is =>  " + responseBody);
	int StatusCode = response.statusCode();
	return StatusCode; 
	}
	
}
