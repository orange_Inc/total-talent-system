package runConfig;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/Feature",
		tags = {"@API"},
//		 dryRun = false,
		glue = { "stepDefinition" },
				monochrome = true,
		plugin = { "pretty", "html:target/Destination" })

public class RunTest {
}
