package stepDefinition;

import com.api.get.GetBase;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import junit.framework.Assert;

@SuppressWarnings("deprecation")
public class GetSystemValues {

	
	@Given("^Check the get method for the System value \"([^\"]*)\"$")
	public void check_the_get_method_for_the_System_value(String arg1) throws Throwable {
		String resp = GetBase.GetResposeBody(arg1);
		System.out.println(resp);
		
	
	}

	@Then("^Should reply the following status Code (\\d+) for this method \"([^\"]*)\"$")
	public void should_reply_the_following_status_Code_for_this_method(int arg1, String arg2) throws Throwable {
		Assert.assertEquals(arg1, GetBase.Getvalues(arg2));
	}


}
